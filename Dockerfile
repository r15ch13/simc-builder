FROM alpine:latest
MAINTAINER Richard Kuhnt <r15ch13@gmail.com>

# Install needed tools
RUN apk update && \
    apk upgrade && \
    apk add \
        ca-certificates \
        curl \
        git \
        g++ \
        libssl1.0 \
        libstdc++ \
        make \
        openssl-dev \
        && \
    rm -rf /var/cache/apk/*
